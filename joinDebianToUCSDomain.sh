#!/bin/bash

# VERSION:
# 0.6.1

# ABOUT:
# This script joins a computer in an UCS domain
# It is based on instructions for Ubuntu originally.
# However, on Debian systems, the package 'auth-client-config' is not available
# But the Ubuntu package works just fine :)

# WARNING:
# This script is not idempotent, so don't run it more than once!!!
# If something goes wrong and you need to run this script again,
# please uninstall it first (see 'USAGE' below)

# USAGE:
#0 . If you want to run this in non-interactive mode you need to setup ssh keys for the below specified user so you can log without password on the domain controller. The user needs to be able to run "ucr" and "udm" commands on the MDC
# 1. Fill in all the variables in the VARS section below before running!
# 2. You need to run this script as root or with the sudo command
# 3. To install run: 'joinUCS.sh install'
# 4. To uninstall run: 'joinUCS.sh uninstall'
# 5. To check domain membership run: 'joinUCS.sh check'

# Credits:
# Original version from here: https://docs.software-univention.de/domain-4.2.pdf
# Extended by Stefan Heil / heil@tu-berlin.de

################################################################################

# The IP address of the UCS master domain controller
DOMAIN_CONTROLLER_MASTER="192.168.1.240"

# The name of the domain administrator account, usually "Administrator"
DOMAIN_ADMIN="Administrator"

# The name of the domain admin group, usually "Domain Admins"
DOMAIN_ADMIN_GROUP="Domain Admins"

# Username with privileges to use the "ucr" and "udm" utilities on the domain controller.
# It is advised to create a service account on the MDC which can use "ucr" and "udm" but does not have "root" privileges
USER="root"

# The name of your domain, e.g. 'example.net'
DOMAIN_NAME="home.lab"

# Set this to the name of the UCS network object this host should be in
UCS_NETWORK="default"

# Define group for this machine. In a default UCS installation these exist:
#
# computer, domaincontroller_slave, macos, ubuntu, windows_domaincontroller,
# domaincontroller_backup, ipmanagedclient, memberserver, ucc,
# domaincontroller_master, linux, trustaccount, windows
DOMAIN_COMPUTER_GROUP="linux"

# Specify the network interface name used to connect to the domain controller
IFACE="eth0"

# If 'true', the script will tell DHCP server to use our current IP of ${IFACE}
STATIC_IP='false'

# Comma separated list of LDAP users and groups that are allowed to login
SIMPLE_ALLOW_USERS="${DOMAIN_ADMIN}"
SIMPLE_ALLOW_GROUPS="${DOMAIN_ADMIN_GROUP}"


################################################################################
# You should not have to change anything below this line!
################################################################################


INIT(){

    if [ "$(whoami)" != 'root' ]; then
        MSG ERROR "You need to run this script as root (or with sudo)!"
    fi

    # Check for our helper script for detection of operating system, distro & version
    if [[ -f helperScripts/master.sh ]]
    then
        . helperScripts/master.sh
        # This provides the variables "OS" and "OS_VERSION"
        getOS
        MSG SUCCESS "Found master.sh helper script."
    else
        echo "This script depends on 'master.sh' which is not installed.\n\
        Please install it from the repository."
    fi

    if [[ $OS == "Ubuntu" ]] ||  [[ $OS == "Debian" ]]
    then
        # Install auth-client-config // FIXME: works only for Ubuntu
        MSG SUCCESS "Operating system is '${OS}'. Continuing..."
    else
        MSG INFO "Your Operating System '${OS}' is not supported. Aborting..."
        exit 1
    fi

	export DOMAIN_CONTROLLER_MASTER
	export DOMAIN_ADMIN
	export DOMAIN_ADMIN_GROUP
	export ROOT
	export DOMAIN_NAME
	export UCS_NETWORK
	export DOMAIN_COMPUTER_GROUP
	export IFACE
	export STATIC_IP
	export SIMPLE_ALLOW_USERS
	export SIMPLE_ALLOW_GROUPS

	# Source our config file if it already exists
	if [[ -f /etc/univention/ucr_master ]]
	then
		. /etc/univention/ucr_master
	fi

	# Create an account and save the password
	export PASSWORD="$(tr -dc A-Za-z0-9_ </dev/urandom | head -c20)"

	# Get the mac address of ${IFACE}
	export MAC=$(cat /sys/class/net/${IFACE}/address)

	# Get the IP address of ${IFACE}
	export IP=$(ifconfig ${IFACE} | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p')
	# TODO: use "ip" for derivation of IP address since ifconfig (net-tools package) is not installed by default on Debian >=9 and Ubuntu >=17

	# Get the last part of our IP address (eg. '185' for IP '192.168.1.185')
	export RDNS_HOST=$(echo ${IP} | awk -F. '{print $4}')

	# Get the reverse DNS zone (eg. '1.168.192' for network '192.168.1.0/24')
	export RDNS_ZONE=$(echo ${IP} | awk -F. '{print $3"." $2"."$1}')

	# Our hostname
	export HOSTNAME=$(hostname)

	export DHCP="cn=${DOMAIN_NAME},cn=dhcp,${ldap_base}"

	export NETWORK="cn=${UCS_NETWORK},cn=networks,${ldap_base}"

}


SSH_KEY_SETUP(){

    ACTION=$1

    SSH_PUBKEY=$(getent passwd ${USER} | cut -d: -f6)/.ssh/id_rsa.pub
    SSH_PRIVKEY=$(getent passwd ${USER} | cut -d: -f6)/.ssh/id_rsa

	if [[ -f $SSH_PUBKEY ]]
	then
        MSG SUCCESS "SSH PUBLIC KEY FOUND"
	else
        MSG ERROR "NO SSH PUBLIC KEY FOUND"
        MSG INFO "GENERATING A NEW SSH KEY PAIR FOR ${USER}"
        ssh-keygen -t rsa -N "" -C "" -f $SSH_PRIVKEY
    fi

    case ${ACTION} in
        install)
            MSG INFO "Copying SSH pubkey to ${DOMAIN_CONTROLLER_MASTER}/.ssh/authorized_keys"
            MSG INFO "You will be asked to enter ther password for the user: ${USER}"
            ssh-copy-id ${USER}@${DOMAIN_CONTROLLER_MASTER} > /dev/null 2>&1
            ;;

        uninstall)
            MSG INFO "Uninstalling SSH pubkey from ${USER}@${DOMAIN_CONTROLLER_MASTER}/.ssh/authorized_keys"
            idssh=`cat ${SSH_PUBKEY} | awk '{print $2}'`
            ssh ${USER}@${DOMAIN_CONTROLLER_MASTER} "sed -i '\_${idssh}_{d}' .ssh/authorized_keys" || MSG ERROR "Could not uninstall the SSH key"
            ;;
    esac
}

INSTALL(){

    INIT

    # Source our config file
    if [[ -f /etc/univention/ucr_master ]]
    then
        MSG ERROR "/etc/univention/ucr_master exists. You need to use 'uninstall' option first!"
    fi

    # Use SSH keys so we don't have to prompt the user for the password repeatedly
    SSH_KEY_SETUP install

    # Create a local directory to store important files
    mkdir /etc/univention

    # Get the hostname of the domain controller and store it
    ssh -n ${USER}@${DOMAIN_CONTROLLER_MASTER} 'ucr shell | grep -v ^hostname=' > /etc/univention/ucr_master
    echo "master_ip=${DOMAIN_CONTROLLER_MASTER}" >> /etc/univention/ucr_master
    chmod 660 /etc/univention/ucr_master

    # Source the file content
    . /etc/univention/ucr_master

    # Add the domain controller to our hosts file for name resolution
    echo "${DOMAIN_CONTROLLER_MASTER} ${ldap_master}" >> /etc/hosts

    # Download the SSL certificate
    mkdir -p /etc/univention/ssl/ucsCA/
    wget -O /etc/univention/ssl/ucsCA/CAcert.pem http://${ldap_master}/ucs-root-ca.crt >/dev/null 2>&1

    # Set some more variables
    export DHCP="cn=${DOMAIN_NAME},cn=dhcp,${ldap_base}"
    export NETWORK="cn=${UCS_NETWORK},cn=networks,${ldap_base}"

    CREATE_MACHINE_ACCOUNT
    LDAP
    SSSD
    PAM
    KERBEROS

    # Delete the SSH key again from server for security reasons
    SSH_KEY_SETUP uninstall
}

CREATE_MACHINE_ACCOUNT(){

    MSG INFO "Creating machine account for host ${HOSTNAME}"

    MSG SUCCESS "$(ssh -n ${USER}@${ldap_master} udm computers/${DOMAIN_COMPUTER_GROUP} create \
        --position "'cn=computers,${ldap_base}'" \
        --set name="'${HOSTNAME}'" \
        --set password="'${PASSWORD}'" \
        --set operatingSystem="'${OS}'" \
        --set operatingSystemVersion="'${OS_VERSION}'" \
        --set network="'${NETWORK}'" \
        --set mac="'${MAC}'" \
        --set ip="'${IP}'"  \
        --set dnsEntryZoneReverse="'zoneName=${RDNS_ZONE}.in-addr.arpa,cn=dns,${ldap_base} ${IP}'" \
        --set domain="'${DOMAIN_NAME}'" || MSG ERROR "could not create domain machine account '${HOSTNAME}'" \
#       --set dhcpEntryZone="'${DHCP} ${IP} ${MAC}'" \ # if this is set, the DNS entrys and the DHCP entry are added automatically!
    )"

    MSG INFO "Modifying DNS records for host ${HOSTNAME}"

    MSG SUCCESS "$(ssh -n ${USER}@${ldap_master} udm dns/host_record modify \
        --dn="relativeDomainName=${HOSTNAME},zoneName=${DOMAIN_NAME},cn=dns,${ldap_base}" \
        --set a="${IP}" || MSG ERROR "could not modify dns host record for '${HOSTNAME}'" \
    )"

    MSG INFO "Creating DNS PTR record for host ${HOSTNAME}"

    MSG SUCCESS "$(ssh -n ${USER}@${ldap_master} udm dns/ptr_record create \
        --superordinate="zoneName=${RDNS_ZONE}.in-addr.arpa,cn=dns,${ldap_base}" \
        --set address=${RDNS_HOST} \
        --set ptr_record=${HOSTNAME}.${DOMAIN_NAME} || MSG ERROR "could not create PTR record for ${HOSTNAME}.${DOMAIN_NAME}" \
    )"

    # Store our random password from above as our ldap secret and set strict permissions
    printf '%s' "${PASSWORD}" >/etc/ldap.secret
    chmod 0400 /etc/ldap.secret

    # Dynamic or static IP address?
    if [[ "${STATIC_IP}" == 'false' ]]
    then
        # If IP address is dynamic, add the needed DHCP objects
        MSG INFO "Creating a DHCP object for host ${HOSTNAME} with IP address ${IP}"

        MSG SUCCESS "$(ssh -n ${USER}@${ldap_master} udm dhcp/host create \
            --dn "cn=${HOSTNAME},${DHCP}" \
            --position "${DHCP}" \
            --set host="${HOSTNAME}" \
            --set fixedaddress="${IP}" \
            --policy-reference="cn=default-settings,cn=boot,cn=dhcp,cn=policies,${ldap_base}" \
            --set hwaddress="'ethernet ${MAC}'" || MSG ERROR "could not create dhcp object" \
        )"

        #MSG SUCCESS "$(ssh -n ${USER}@${ldap_master} udm dhcp/host modify \
        # --dn="cn=${HOSTNAME},${DHCP}" \
        # --policy-reference="cn=default-settings,cn=boot,cn=dhcp,cn=policies,${ldap_base}" || MSG ERROR "could not modify dhcp host record for '${HOSTNAME}'" \
        #)"

        # create a dhcp boot policy for the dhcp/host object
        #MSG INFO "$(ssh -n ${USER}@${ldap_master} udm policies/dhcp_boot create --position "cn=boot,cn=dhcp,cn=policies,${ldap_base}" \
        #  --set name="$client" \
        #  --set boot_server="$boot_server" \
        #  --set boot_filename="$boot_filename" \
        #)"

        # show policy settings für dhcp_host object
        #udm dhcp/host list  --superordinate "cn=${DOMAIN_NAME},cn=dhcp,${ldap_base}" --policies 0
    fi

}

LDAP(){
# Create our local ldap config file

# DO NOT USE INDENTATION HERE (because we use a 'here doc')
cat > /etc/ldap/ldap.conf <<__EOF__
TLS_CACERT /etc/univention/ssl/ucsCA/CAcert.pem
URI ldap://$ldap_master:7389
BASE ${ldap_base}
__EOF__

}

SSSD(){
# Configuration of the System Security Services Daemon (SSSD)

#. /etc/univention/ucr_master

# Install SSSD based configuration
INSTALL_PKG 'sssd libnss-sss libpam-sss libsss-sudo'

if [[ $OS == "Ubuntu" ]]
then
    # Install auth-client-config // FIXME: works only for Ubuntu
    INSTALL_PKG 'auth-client-config'
elif [[ $OS == "Debian" ]]
then
    # For Debian download package from latest Ubuntu
    wget -O /root/auth-client-config_0.9ubuntu1_all.deb http://archive.ubuntu.com/ubuntu/pool/universe/a/auth-client-config/auth-client-config_0.9ubuntu1_all.deb >/dev/null 2>&1
    dpkg -i /root/auth-client-config_0.9ubuntu1_all.deb >/dev/null 2>&1

    # Maybe a better Debian Way??
    # https://help.univention.com/t/debian-an-ucs/2164/5
    # https://wiki.debian.org/AuthenticatingLinuxWithActiveDirectory
    # Install LDAP Pam Bindings and more stuff
    #DEBIAN_FRONTEND=noninteractive apt-get install -y libpam-ldapd libnss-ldapd nslcd nscd

    # WARNING, THIS MIGHT BE BUGGY ON YOUR SYSTEM!
    # The config file /usr/share/pam-configs/sss that ships with libpam-sss seems to only work for Ubuntu?!?
    # The priority in the shipped config is 128, but then local passwords can't be changed anymore.
    # This needs to be set to 256 (same as /usr/share/pam-configs/unix) on Debian systems!
    # see: https://bugs.launchpad.net/ubuntu/+source/sssd/+bug/957486
    sed -i "s/128/256/" /usr/share/pam-configs/sss
else
    MSG INFO "Your Operating System '$OS' is not supported."
    MSG INFO "You need to configure 'auth-client-config' or sssd yourself"
fi

# Create sssd.conf
# DO NOT USE INDENTATION HERE (because we use a 'here doc')
#mkdir /etc/sssd/
cat > /etc/sssd/sssd.conf <<__EOF__
[sssd]
config_file_version = 2
reconnection_retries = 3
sbus_timeout = 30
services = nss, pam, sudo
domains = $kerberos_realm

[nss]
reconnection_retries = 3

[pam]
reconnection_retries = 3

[domain/$kerberos_realm]
auth_provider = krb5
krb5_kdcip = ${master_ip}
krb5_realm = ${kerberos_realm}
krb5_server = ${ldap_master}
krb5_kpasswd = ${ldap_master}
id_provider = ldap
ldap_uri = ldap://${ldap_master}:7389
ldap_search_base = ${ldap_base}
ldap_tls_reqcert = never
ldap_tls_cacert = /etc/univention/ssl/ucsCA/CAcert.pem
cache_credentials = true
enumerate = true
ldap_default_bind_dn = cn=${HOSTNAME},cn=computers,${ldap_base}
ldap_default_authtok_type = password
ldap_default_authtok = $(cat /etc/ldap.secret)

# Use 'simple' auth to limit Login to these accounts or groups
access_provider = simple
simple_allow_users = ${SIMPLE_ALLOW_USERS}
simple_allow_groups = ${SIMPLE_ALLOW_GROUPS}
__EOF__

chmod 600 /etc/sssd/sssd.conf


# Create an auth config profile for sssd
# DO NOT USE INDENTATION HERE (because we use a 'here doc')
cat > /etc/auth-client-config/profile.d/sss <<__EOF__
[sss]
nss_passwd= passwd: compat sss
nss_group= group: compat sss
nss_shadow= shadow: compat
nss_netgroup= netgroup: nis
pam_auth=
 auth [success=3 default=ignore] pam_unUbuntuix.so nullok_secure try_first_pass
 auth requisite pam_succeed_if.so uid >= 500 quiet
 auth [success=1 default=ignore] pam_sss.so use_first_pass
 auth requisite pam_deny.so
 auth required pam_permit.so
pam_account=
 account required pam_unix.so
 account sufficient pam_localuser.so
 account sufficient pam_succeed_if.so uid < 500 quiet
 account [default=bad success=ok user_unknown=ignore] pam_sss.so
 account required pam_permit.so
pam_password=
 password requisite pam_pwquality.so retry=3
 password sufficient pam_unix.so obscure sha512
 password sufficient pam_sss.so use_authtok
 password required pam_deny.so
pam_session=
 session required pam_mkhomedir.so skel=/etc/skel/ umask=0077
 session optional pam_keyinit.so revoke
 session required pam_limits.so
 session [success=1 default=ignore] pam_sss.so
 session required pam_unix.so
__EOF__

auth-client-config -a -p sss

# Restart sssd
# The commands getent passwd and getent group should afterwards also display all users and groups of the UCS domain.
service sssd restart
}

PAM(){

#############
# Configuring user logins
# DO NOT USE INDENTATION HERE (because we use a 'here doc')
cat > /usr/share/pam-configs/ucs_mkhomedir <<__EOF__
Name: activate mkhomedir
Default: yes
Priority: 900
Session-Type: Additional
Session: required pam_mkhomedir.so umask=0022 skel=/etc/skel
__EOF__

DEBIAN_FRONTEND=noninteractive pam-auth-update --force


# During login users should also be added to some system groups:
echo '*;*;*;Al0000-2400;audio,cdrom,dialout,floppy,plugdev,adm' >>/etc/security/group.conf

# DO NOT USE INDENTATION HERE (because we use a 'here doc')
cat > /usr/share/pam-configs/local_groups <<__EOF__
Name: activate /etc/security/group.conf
Default: yes
Priority: 900
Auth-Type: Primary
Auth: required pam_group.so use_first_pass
__EOF__

DEBIAN_FRONTEND=noninteractive pam-auth-update --force

if [[ -d /etc/lightdm ]]
then
    # Fix LightDM Menu
    # Add a field for a user name, disable user selection at the login screen
    # It is currently not possible to change the user password at the LightDM login manager. Instead, the password can be changed via the kpasswd command after login or via the UMC module Change password.

    # Kubuntu 14.04 uses AccountService, a D-Bus interface for user account management, which ignores the /etc/lightdm.conf file. Since there is no configuration file for AccountService the login theme needs to be changed to classic under System Settings -> Login Screen (LightDM). With these settings the login for domain members should be possible after a restart of LightDM or a reboot.
    mkdir /etc/lightdm/lightdm.conf.d

# DO NOT USE INDENTATION HERE (because we use a 'here doc')
cat >>/etc/lightdm/lightdm.conf.d/99-show-manual-userlogin.conf
 <<__EOF__
[SeatDefaults]
greeter-show-manual-login=true
greeter-hide-users=true
__EOF__

fi
}

KERBEROS(){
###########
# Kerberos Integration

#. /etc/univention/ucr_master
# Install required packages
INSTALL_PKG 'heimdal-clients ntpdate'

# Default krb5.conf
# DO NOT USE INDENTATION HERE (because we use a 'here doc')
cat > /etc/krb5.conf <<__EOF__
[libdefaults]
 default_realm = $kerberos_realm
 kdc_timesync = 1
 ccache_type = 4
 forwardable = true
 proxiable = true
 default_tkt_enctypes = arcfour-hmac-md5 des-cbc-md5 des3-hmac-sha1 des-cbc-crc des-cbc-md4 des3-cbc-sha1 aes128-cts-hmac-sha1-96 aes256-cts-hmac-sha1-96
 permitted_enctypes = des3-hmac-sha1 des-cbc-crc des-cbc-md4 des-cbcmd5 des3-cbc-sha1 arcfour-hmac-md5 aes128-cts-hmac-sha1-96 aes256-ctshmac-sha1-96
 allow_weak_crypto=true

[realms]
 $kerberos_realm = {
 kdc = $master_ip $ldap_master
 admin_server = $master_ip $ldap_master
 kpasswd_server = $master_ip $ldap_master
}
__EOF__

# MSG INFO $(net rpc join -U Administrator%root123)

# Synchronize the time with the UCS system
MSG INFO "Updating time from domain NTP server '$ldap_master'"
ntpdate -bu $ldap_master >/dev/null 2>&1 || MSG ERROR "updating time from domain NTP server"

MSG SUCCESS "Host '${HOSTNAME}' was added to UCS '${DOMAIN_NAME}'!"

}

CHECK(){

    # Initialize our variables
#    INIT

    # Test Kerberos: kinit will ask you for a ticket and the SSH login to the master should work with ticket authentication:
    if [[ -x $(which kinit) ]]; then
        printf ">>> Please enter "
        kinit $DOMAIN_ADMIN
    else
        MSG ERROR "kinit is not installed, so domain support is not enabled yet"
        exit 1
    fi

    if [[ $? -eq 0 ]]
    then
        printf "\n"
        MSG INFO "Trying kerberos auth with the domain controller master: "
        ssh -n $DOMAIN_ADMIN@$ldap_master ls /etc/univention > /dev/null 2>&1
    else
        exit 1
    fi

    if [[ $? -eq 0 ]]
    then
        MSG SUCCESS "Kerberos Authentication working :)"
    else
        MSG ERROR "Kerberos Authentication NOT working :("
        exit 1
    fi

    # Destroy the kerberos ticket
    kdestroy

    # Test LDAP
    if [[ -x $(which ldapsearch) ]]
    then
        #printf "The ldapsearch utility was found and is executable\n\n"
        printf ""
    else
        MSG INFO "The 'ldapsearch' utility was not found. Installing it for you now"
        INSTALL_PKG 'ldap-utils'
    fi

    # Sleep one second for better optical recognition of shell output
    sleep 1
    MSG INFO "Querying LDAP server at ${ldap_master}: ldapsearch -s sub -D cn=${HOSTNAME},cn=computers,${ldap_base} -w ******** '(name=${HOSTNAME})'"
    ldapsearch -s sub -D cn=${HOSTNAME},cn=computers,${ldap_base} -w $(cat /etc/ldap.secret) "(name=${HOSTNAME})" > /dev/null 2>&1 || MSG ERROR "LDAP access NOT working or account doesn't exist."

    if [[ $? -eq 0 ]]
    then
        MSG SUCCESS "Host '${HOSTNAME}' is a member of domain '${DOMAIN_NAME}'!"
    else
        MSG ERROR "LDAP access NOT working or account doesn't exist."
    fi

}

DELETE_MACHINE_ACCOUNT(){

    MSG INFO "Deleting machine account for host ${HOSTNAME}"

    MSG SUCCESS "$(ssh -n ${USER}@${ldap_master} udm computers/${DOMAIN_COMPUTER_GROUP} remove --dn "cn=${HOSTNAME},cn=computers,${ldap_base}" || MSG ERROR "could not remove computer object from domain")"

    # Remove the computer object
    MSG INFO "Deleting DNS HOST record for host ${HOSTNAME}"

    MSG SUCCESS "$(ssh -n ${USER}@${ldap_master} udm dns/host_record remove --dn="relativeDomainName=${HOSTNAME},zoneName=${DOMAIN_NAME},cn=dns,${ldap_base}" || MSG ERROR "could not remove HOST record for ${HOSTNAME}.${DOMAIN_NAME}")"

    MSG INFO "Deleting DNS PTR record for host ${HOSTNAME}"

    MSG SUCCESS "$(ssh -n ${USER}@${ldap_master} udm dns/ptr_record remove --dn="relativeDomainName=${RDNS_HOST},zoneName=${RDNS_ZONE}.in-addr.arpa,cn=dns,${ldap_base}" || MSG ERROR "could not remove PTR record for ${HOSTNAME}.${DOMAIN_NAME}")"

    # Dynamic or static IP address?
    if [[ "${STATIC_IP}" == "false" ]]
    then
        # If IP address is dynamic, also remove the DHCP object
        #MSG INFO "Removing DHCP boot policy for host ${HOSTNAME}"

        #MSG SUCCESS "$(ssh -n ${USER}@${ldap_master} udm policies/dhcp_boot remove --dn "cn=${HOSTNAME},cn=boot,cn=dhcp,cn=policies,${ldap_base}" || MSG ERROR "could not remove dhcp boot policy for ${HOSTNAME}.${DOMAIN_NAME}")"

        MSG INFO "Removing DHCP object for host ${HOSTNAME}"

        MSG SUCCESS "$(ssh -n ${USER}@${ldap_master} udm dhcp/host remove --superordinate ${DHCP} --dn "cn=${HOSTNAME},${DHCP}" || MSG ERROR "could not remove DHCP object from domain")"

        # remove dhcp_boot policy for $client
        #MSG INFO "$(
        #    ssh -n ${USER}@${ldap_master} udm policies/dhcp_boot remove --dn "cn=${HOSTNAME},cn=boot,cn=dhcp,cn=policies,${ldap_base}"
        #)"
    fi

}

UNINSTALL(){

    INIT

    if [[ -f /etc/univention/ucr_master ]]; then

        MSG INFO "/etc/univention/ucr_master found, starting removal process ..."
        . /etc/univention/ucr_master
        # Initialize our variables
#        INIT

        export DHCP="cn=${DOMAIN_NAME},cn=dhcp,${ldap_base}"
        export NETWORK="cn=${UCS_NETWORK},cn=networks,${ldap_base}"
        #export HOSTNAME=$(hostname)
        #export OS=$(lsb_release -is)
        #export OS_VERSION=$(lsb_release -rs)
    else
        MSG ERROR "/etc/univention/ucr_master does not exist, can't continue :("
    fi

    # Delete the SSH key again from server for security reasons
    SSH_KEY_SETUP install

	DELETE_MACHINE_ACCOUNT

    # Remove installed packages and purge config files
    RM_PKG="auth-client-config heimdal-clients sssd libnss-sss libpam-sss libsss-sudo"
    MSG INFO "Uninstalling software $RM_PKG"
    DEBIAN_FRONTEND=noninteractive apt-get -qq -y remove --purge $RM_PKG 1>/dev/null
    #DEBIAN_FRONTEND=noninteractive apt-get -qq -y autoremove

    MSG INFO "Deleting all remaining configuration files"
    rm -rf /etc/univention/ \
     /etc/ldap/ldap.conf \
     /etc/sssd/sssd.conf \
     /root/auth-client-config_0.9ubuntu1_all.deb \
     /etc/auth-client-config \
     /usr/share/pam-configs/ucs_mkhomedir \
     /usr/share/pam-configs/local_groups \
     /etc/krb5.conf

    # Remove the entry for the groups from /etc/security/group.conf
    sed -i "s/'\*;\*;\*;Al0000-2400;audio,cdrom,dialout,floppy,plugdev,adm'//" /etc/security/group.conf

    # Delete the entry from the hosts file
    sed -i "/${DOMAIN_CONTROLLER_MASTER} ${ldap_master}/d" /etc/hosts

    # Delete the /etc/univention/ucr_master
    rm -f /etc/univention/

    MSG SUCCESS "Removal of host '${HOSTNAME}' from UCS '${DOMAIN_NAME}' complete!"

    # Delete the SSH key again from server for security reasons
    SSH_KEY_SETUP uninstall
}


# Trap the 'exit 1' event within functions.
# Otherwise the functions exits, but the main script will continue after error.
trap "exit 1" TERM
export TOP_PID=$$

case "$1" in

    install)
        printf "\n"
        printf "#######################################\n"
        printf "#  Installing UCS Domain support ...  #\n"
        printf "#######################################\n\n"
        INSTALL
        ;;

    uninstall)
        printf "\n"
        printf "#######################################\n"
        printf "#   Removing UCS Domain support ...   #\n"
        printf "#######################################\n\n"
        UNINSTALL
        ;;

    check)
        printf "\n"
        printf "#######################################\n"
        printf "#   Testing UCS Domain support ...    #\n"
        printf "#######################################\n\n"
        CHECK
        ;;

    *)
        echo "Specify either 'install', 'uninstall' or 'check'"
        ;;
esac
