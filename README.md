# joinDebianToUCSDomain.sh

## Description

This shell script will join a Debian based system into a Univention Corporate Server (UCS) Domain. It handles installation of relevant packages and creates the necessary objects on the UCS Master Domain Controller (MDC).

The reason for writing this script is that the Univention supplied Ubuntu package ([Univention Domain Join](https://github.com/univention/univention-domain-join)) supports only Ubuntu and Mint and I needed to join a few original Debian systems. Another advantage is that this script also works on armHF systems (e.g. Raspberry Pis)

The script is based on the official Univention [Extended domain services documentation v4.2](https://docs.software-univention.de/domain-4.2.pdf)

## Compatibility

Tested on the following distributions and architectures

Works on:
- Debian 10.0 (Buster) / amd64
- Debian 9.2 (Stretch) / amd64
- Debian 9.1 (Stretch) / amd64
- Debian 9.1 (Stretch) / armhf
- Debian 8.9 (Jessie) / amd64
- Ubuntu 14.04.5 LTS (Trusty Tahr) / amd64

Does not work on:
- Debian 8.9 (Jessie) / armhf --> Problems with sssd

If you trie this script on another distro / architecture let me know so I can update this list.

## Usage

The script needs to be adapted to your specific environment by setting a few variables in the beginning of the script. The script is commented so it should be self-explanatory. If not, create an issue here.

### Installation

`./joinDebianToUCSDomain.sh install`

### Removal

`./joinDebianToUCSDomain.sh uninstall`

### Check join status

`./joinDebianToUCSDomain.sh check`


### Automatic / non-interactive mode:

- If the SSH key of the user specified in the script ("USER") is installed on the MDC, then non-interactive mode should work. 
- If the SSH key is not yet installed, the script will promt for the password to ssh-copy-id the keys over to the MDC. 
- When the installation/uninstallation is finished successfully, the script will delete the SSH keys again from the server! This is to make sure that the system can't access the MDC afterwards.
- Pay close attention when the script fails to make sure no keys are left on the server unintendedly.


## WARNING

The script is not idempotent (yet), so if you run into problems uninstall before installing again. The uninstaller should handle the removal of all LDAP objects, but if there is a problem you might have to remove remnants by hand (on the UCS MDC, either from the web-based management console or with the `udm` command line utility. Sorry :(
